package com.example.patito.controllers;

import com.example.patito.input.patitoInput;
import com.example.patito.model.domain.Patito;
import com.example.patito.services.patitoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin(origins = "*", methods = {RequestMethod.GET, RequestMethod.POST, RequestMethod.PUT, RequestMethod.DELETE})
public class patitoController {

    @Autowired
    private patitoService patitoService;

    @RequestMapping("/patitos")
    public List<Patito> obtenerPatitos(){
        return patitoService.getPatito();
    }
    @RequestMapping( method = RequestMethod.POST, value = "/patitos/post")
    public long addPerson(@RequestBody patitoInput patitoInput) {
        Patito patito = new Patito();
        patito.setColor(patitoInput.getColor());
        patito.setTamanio(patitoInput.getTamaño());
        patito.setPrecio(patitoInput.getPrecio());
        patito.setCantidad(patitoInput.getCantidad());
        patito.setBorrado(patitoInput.isBorrado());
        patitoService.addPato(patito);
        return patito.getId();
    }

    @PutMapping("/patitos/put/{id}")
    public void updatePostulantes(@RequestBody Patito patito, @PathVariable long id) {
        patitoService.updatePatito(id, patito);
    }

    @DeleteMapping("/patitos/delete/{id}")
    public void deleteAuxiliary(@PathVariable long id) {
        patitoService.deletePatito(id);
    }



    @RequestMapping("/saludo")
    public void saludo(){
        patitoService.saludo();
    }
}
