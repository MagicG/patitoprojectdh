package com.example.patito.model.domain;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import lombok.Data;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Table;
import org.springframework.data.annotation.Id;

@Data
@Entity
@Table(appliesTo = "patito")
public class Patito {

    @jakarta.persistence.Id
    @Id @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name="Id")
    private long id;

    public void setId(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    @Column(name = "Color")
    private String color;

    @Column(name = "Tamanio")
    private String tamanio;

    @Column(name = "Precio")
    private double precio;

    @Column(name = "Cantidad")
    private int cantidad;

    @Column(name = "Borrado")
    private boolean borrado;

}
