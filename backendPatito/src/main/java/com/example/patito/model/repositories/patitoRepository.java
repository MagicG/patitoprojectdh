package com.example.patito.model.repositories;


import com.example.patito.model.domain.Patito;
import jakarta.transaction.Transactional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface patitoRepository extends JpaRepository<Patito, Long> {

    @Query("select item from Patito item where item.borrado = false order by item.cantidad")
    List<Patito> FindById();

    @Modifying @Transactional
    @Query("update Patito item set item.color = :color, item.tamanio= :tamanio, item.precio = :precio, item.cantidad = :cantidad," +
            "item.borrado = :borrado where item.id = :id")
    void UpdateByCi(@Param("id") long id, @Param("color") String color, @Param("tamanio") String tamanio,@Param("precio") double precio,
    @Param("cantidad") int cantidad, @Param("id") boolean borrado);

    @Modifying @Transactional
    @Query("update Patito item set item.borrado = true where item.id = :id")
    void DeleteById(@Param("id") long id);

    //List<Patito> getAllByBorradoFalse();

}
