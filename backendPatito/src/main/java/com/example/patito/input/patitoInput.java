package com.example.patito.input;

import lombok.Data;

@Data
public class patitoInput {

    private String color, tamaño;
    private double precio;
    private int cantidad;
    private boolean borrado;

}
