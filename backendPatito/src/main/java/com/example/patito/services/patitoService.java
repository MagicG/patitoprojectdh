package com.example.patito.services;

import com.example.patito.model.domain.Patito;
import com.example.patito.model.repositories.patitoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class patitoService {

    @Autowired
    private patitoRepository patitoRepository;


    public List<Patito> getPatito() {
        //return patitoRepository.getAllByBorradoFalse();
        return patitoRepository.FindById();
    }

    public String saludo() {
        return "Hola worlddddd";
    }

    public void addPato(Patito patito) {
        patitoRepository.save(patito);
    }

    public void updatePatito(long id, Patito patito) {
        patitoRepository.UpdateByCi(id, patito.getColor(), patito.getTamanio(), patito.getPrecio(), patito.getCantidad(), patito.isBorrado());
    }

    public void deletePatito(long id) {
        patitoRepository.DeleteById(id);
    }
}
