import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class PatitoService {

  constructor(private http:HttpClient) { }

  getPatitos(){
    console.log("Hola mundo aqui desde Services.msc");  
    return this.http.get("http://localhost:8010/patitos");
        
  }
}
