import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FormularioPatitoComponent } from './formulario-patito.component';

describe('FormularioPatitoComponent', () => {
  let component: FormularioPatitoComponent;
  let fixture: ComponentFixture<FormularioPatitoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FormularioPatitoComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(FormularioPatitoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
